﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace rinput_tool
{
    public partial class ManageRInputs : Form
    {
        public ManageRInputs()
        {
            InitializeComponent();
        }

        public void setNames(String[] names)
        {
            list.Items.Clear();
            foreach (String name in names)
                list.Items.Add(name);
        }

        private void ManageRInputs_Shown(object sender, EventArgs e)
        {
            btnRemove.Enabled = !(this.list.SelectedItems.Count == 0);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            string[] items = list.SelectedItems.Cast<string>().ToArray();
            foreach (string item in items) list.Items.Remove(item);
            Program.updateProcesses(list.Items.Cast<string>().ToArray());
        }

        public void addEntry(string name)
        {
            list.Items.Add(name);
            Program.updateProcesses(list.Items.Cast<string>().ToArray());
        }

        private void btnAddPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog dia = new OpenFileDialog();
            dia.AddExtension = true;
            dia.AutoUpgradeEnabled = true;
            dia.CheckFileExists = true;
            dia.DefaultExt = "exe";
            dia.Filter = "Application files (*.exe)|*.exe";
            DialogResult res = dia.ShowDialog();
            if (res == DialogResult.OK)
                addEntry(Path.GetFileName(dia.FileName));
        }

        private void list_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = !(list.SelectedItems.Count == 0);                
        }

        private void btnAddManual_Click(object sender, EventArgs e)
        {
            var name = TextPrompt.ShowDialog("Please enter the program name (e.g. quake3.exe):", "Manually add program", null);
            if (name == null) return;
            addEntry(name);
        }
    }
}

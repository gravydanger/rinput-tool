﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace rinput_tool
{
    enum InjectResult {
        OK,
        ALREADY_DONE,
        RETRY,
        GET_MODULES_FAILED,
        PROCESS_DISAPPEARED,
        OPEN_PROCESS_FAILED,
        GET_LOADLIBRARY_ADDR_FAILED,
        ALLOCATE_DLLNAME_FAILED,
        WRITE_DLLNAME_FAILED,
        CREATE_THREAD_FAILED,
        WAIT_THREAD_FAILED,
        THREAD_GET_RESULT_FAILED,
        LOAD_LIBRARY_FAILED,
        GET_ENTRY_POINT_ADDRESS_FAILED,
        CREATE_EVENT_FAILED,
        CREATE_ENTRY_POINT_THREAD_FAILED,
        WAIT_EVENT_FAILED,
    };

    class InjectException : Exception
    {
        public InjectResult result;

        public InjectException(InjectResult result) : base()
        {
            this.result = result;
        }
    }

    static class Injector
    {
        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("kernel32.dll", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
        public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        public static extern IntPtr VirtualAllocEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, uint flAllocationType, uint flProtect);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out uint lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern IntPtr CreateRemoteThread(IntPtr hProcess, IntPtr lpThreadAttributes, uint dwStackSize, IntPtr lpStartAddress, IntPtr lpParameter, uint dwCreationFlags, IntPtr lpThreadId);

        [DllImport("kernel32.dll")]
        public static extern uint WaitForSingleObject(IntPtr hProcess, uint dwMilliseconds);

        [DllImport("kernel32.dll")]
        public static extern bool GetExitCodeThread(IntPtr hThread, out IntPtr lpExitCode);

        [DllImport("kernel32.dll")]
        public static extern bool VirtualFreeEx(IntPtr hProcess, IntPtr lpAddress, uint dwSize, uint dwFreeType);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr LoadLibraryEx(string lpFileName, IntPtr hFile, uint dwFlags);

        [DllImport("kernel32.dll")]
        public static extern bool FreeLibrary(IntPtr hModule);

        [DllImport("kernel32.dll", CharSet = CharSet.Ansi)]
        public static extern IntPtr CreateEvent(IntPtr lpEventAttributes, bool bManualReset, bool bInitialState, string lpName);

        [DllImport("kernel32.dll")]
        public static extern bool CloseHandle(IntPtr hObject);

        public static InjectResult Inject(int pid, bool isRetry)
        {
            Process proc;
            try
            {
                proc = Process.GetProcessById(pid);
            } catch (System.ArgumentException)
            {
                return InjectResult.PROCESS_DISAPPEARED;
            }
            try
            {
                foreach (ProcessModule m in proc.Modules)
                {
                    if (m.ModuleName == "rinput.dll") return InjectResult.ALREADY_DONE;
                }
            } catch (System.ComponentModel.Win32Exception)
            {
                if (isRetry) return InjectResult.GET_MODULES_FAILED;
                return InjectResult.RETRY;
            }

            IntPtr procHandle = IntPtr.Zero, allocd = IntPtr.Zero, threadHandle = IntPtr.Zero, eventHandle = IntPtr.Zero;
            IntPtr dllHandle = IntPtr.Zero;
            try
            {
                procHandle = OpenProcess(0x043a, false, pid);
                if (procHandle == IntPtr.Zero) throw new InjectException(InjectResult.OPEN_PROCESS_FAILED);
                var loadLibAddr = GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");
                if (loadLibAddr == IntPtr.Zero) throw new InjectException(InjectResult.GET_LOADLIBRARY_ADDR_FAILED);
                string dllName = Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), "rinput.dll");
                var dllLen = (uint)((dllName.Length + 1) * Marshal.SizeOf(typeof(char)));
                allocd = VirtualAllocEx(procHandle, IntPtr.Zero, dllLen, 0x3000, 4);
                if (allocd == IntPtr.Zero) throw new InjectException(InjectResult.ALLOCATE_DLLNAME_FAILED);
                uint bytesWritten;
                WriteProcessMemory(procHandle, allocd, Encoding.Default.GetBytes(dllName), dllLen, out bytesWritten);
                if (bytesWritten < dllLen) throw new InjectException(InjectResult.WRITE_DLLNAME_FAILED);
                threadHandle = CreateRemoteThread(procHandle, IntPtr.Zero, 0, loadLibAddr, allocd, 0, IntPtr.Zero);
                if (threadHandle == IntPtr.Zero) throw new InjectException(InjectResult.CREATE_THREAD_FAILED);
                var threadWaitRes = WaitForSingleObject(threadHandle, 6500); // value from rinput.exe
                if (threadWaitRes != 0) throw new InjectException(InjectResult.WAIT_THREAD_FAILED);
                IntPtr dllBaseAddress;
                var threadExitResult = GetExitCodeThread(threadHandle, out dllBaseAddress);
                if (!threadExitResult) throw new InjectException(InjectResult.THREAD_GET_RESULT_FAILED);
                CloseHandle(threadHandle);
                threadHandle = IntPtr.Zero;

                // Handle entry point
                // Get relative address of DLL entry point
                dllHandle = LoadLibraryEx(dllName, IntPtr.Zero, 0x1); // DONT_RESOLVE_DLL_REFERENCES
                if (dllHandle == IntPtr.Zero) throw new InjectException(InjectResult.LOAD_LIBRARY_FAILED);
                var entryPointAddr = GetProcAddress(dllHandle, "entryPoint");
                if (entryPointAddr == IntPtr.Zero) throw new InjectException(InjectResult.GET_ENTRY_POINT_ADDRESS_FAILED);
                var remoteEntryPointAddr = new IntPtr(entryPointAddr.ToInt64() - dllHandle.ToInt64() + dllBaseAddress.ToInt64());
                // Create IPC event
                eventHandle = CreateEvent(IntPtr.Zero, false, false, "RInputEvent32");
                if (eventHandle == IntPtr.Zero) throw new InjectException(InjectResult.CREATE_EVENT_FAILED);
                // Call entryPoint function
                threadHandle = CreateRemoteThread(procHandle, IntPtr.Zero, 0, remoteEntryPointAddr, IntPtr.Zero, 0, IntPtr.Zero);
                if (threadHandle == IntPtr.Zero) throw new InjectException(InjectResult.CREATE_ENTRY_POINT_THREAD_FAILED);
                threadWaitRes = WaitForSingleObject(eventHandle, 6500); // value from rinput.exe
                if (threadWaitRes != 0) throw new InjectException(InjectResult.WAIT_EVENT_FAILED);
            } catch (InjectException e)
            {
                return e.result;
            } finally
            {
                if (eventHandle != IntPtr.Zero) CloseHandle(eventHandle);
                if (dllHandle != IntPtr.Zero) FreeLibrary(dllHandle);
                if (threadHandle != IntPtr.Zero) CloseHandle(threadHandle);
                if (allocd != IntPtr.Zero) VirtualFreeEx(procHandle, allocd, 0, 0x8000); // MEM_RELEASE
                if (procHandle != IntPtr.Zero) CloseHandle(procHandle);
            }

            return InjectResult.OK;
        }
    }

    static class Program
    {
        static NotifyIcon icon;
        static MenuItem show;
        static MenuItem autostart;
        static MenuItem exit;
        public static ManageRInputs form;
        static HashSet<String> configured;
        static HashSet<int> handledPids;
        static RegistryKey reg;
        static System.Windows.Forms.Timer timer;

        static ContextMenu initMenu()
        {
            show = new MenuItem("Manage...", m_Show);
            autostart = new MenuItem("Auto-start on boot", m_ToggleAutostart);
            exit = new MenuItem("Exit", m_Exit);
            ContextMenu menu = new ContextMenu();
            menu.MenuItems.Add(show);
            menu.MenuItems.Add(autostart);
            menu.MenuItems.Add(exit);
            return menu;
        }

        static void initReg()
        {
            const string regpath = "Software\\rinput-tool";
            reg = Registry.CurrentUser.OpenSubKey(regpath, true);
            if (null != reg) return;
            reg = Registry.CurrentUser.CreateSubKey(regpath);

            reg.SetValue("Autostart", 1);
            updateAutostart(true);
            reg.CreateSubKey("Instances");
            string[] starterpack = { "jasp.exe", "jamp.exe", "jk2sp.exe", "jk2mp.exe" };
            form.setNames(starterpack);
            updateProcesses(starterpack);
            form.Show();
            MessageBox.Show("Welcome! Add some processes here. The program will keep running in your system tray after you close the manage window.");
        }

        static void fetchReg()
        {
            if ((int) reg.GetValue("Autostart", 0) == 1)
                autostart.Checked = true;
            string[] keys = reg.OpenSubKey("Instances").GetSubKeyNames();
            updateProcesses(keys);
        }

        static void setupMonitor()
        {
            timer = new System.Windows.Forms.Timer();
            timer.Interval = 5000;
            timer.Tick += new EventHandler(OnProcessTimer);
            timer.Start();
        }

        static void showToast(string text, string title = "RInput-tool", int duration = 5000)
        {
            icon.BalloonTipTitle = title;
            icon.BalloonTipText = text;
            icon.ShowBalloonTip(duration);
        }

        static bool handleInjectResult(InjectResult res, string procName)
        {
            if (res == InjectResult.OK || res == InjectResult.ALREADY_DONE) return true;
            var title = "Failed injecting rinput.dll";
            string text;
            switch (res)
            {
                case InjectResult.PROCESS_DISAPPEARED: text = "process exited before we could get to it"; break;
                case InjectResult.OPEN_PROCESS_FAILED: text = "failed opening process"; break;
                case InjectResult.GET_LOADLIBRARY_ADDR_FAILED: text = "failed retrieving entry point for injector"; break;
                case InjectResult.ALLOCATE_DLLNAME_FAILED: text = "failed allocating memory for DLL path"; break;
                case InjectResult.WRITE_DLLNAME_FAILED: text = "failed writing DLL path into process"; break;
                case InjectResult.CREATE_THREAD_FAILED: text = "failed creating injector thread"; break;
                case InjectResult.WAIT_THREAD_FAILED: text = "failed waiting for injector thread to complete"; break;
                case InjectResult.THREAD_GET_RESULT_FAILED: text = "failed retrieving result from injector thread"; break;
                case InjectResult.LOAD_LIBRARY_FAILED: text = "failed loading rinput.dll for determining entry point"; break;
                case InjectResult.GET_ENTRY_POINT_ADDRESS_FAILED: text = "failed getting entry point from rinput.dll"; break;
                case InjectResult.CREATE_EVENT_FAILED: text = "failed creating communication event for rinput.dll"; break;
                case InjectResult.CREATE_ENTRY_POINT_THREAD_FAILED: text = "failed creating entry point thread"; break;
                case InjectResult.WAIT_EVENT_FAILED: text = "rinput.dll did not signal successful load"; break;
                default: text = "unexpected weird error"; break;
            }
            text = string.Format("{0}: {1}", procName, text);
            showToast(text, title);
            return false;
        }

        static List<Process> scan()
        {
            var candidates = new List<Process>();
            var oldPids = new HashSet<int>(handledPids);
            foreach (Process p in Process.GetProcesses())
            {
                string path;
                try
                {
                    path = p.MainModule.ModuleName.ToLower();
                }
                catch (System.ComponentModel.Win32Exception)
                {
                    // Probably access denied
                    continue;
                }
                catch (System.InvalidOperationException)
                {
                    // Probably exited
                    continue;
                }
                catch (System.NullReferenceException)
                {
                    // Probably race condition - module object disappeared
                    continue;
                }
                if (!configured.Contains(path)) continue;
                // Still running, keep in list of handled PIDs
                oldPids.Remove(p.Id);
                if (!handledPids.Contains(p.Id)) candidates.Add(p);
            }
            // All processes still found in oldPids are gone (or no longer match our filters), so remove them from set
            handledPids.ExceptWith(oldPids);
            return candidates;
        }

        static void setupExisting()
        {
            var candidates = scan();
            var injected = new List<Process>();
            foreach (var p in candidates)
            {
                var injectRes = Injector.Inject(p.Id, false);
                if (handleInjectResult(injectRes, p.MainModule.FileName.ToLower())) injected.Add(p);
                // Even if we failed, we don't want to re-scan this over and over
                handledPids.Add(p.Id);
            }
            if (injected.Count == 0) return;
            icon.BalloonTipTitle = "RInput injected into running processes";
            icon.BalloonTipText = "Injected rinput.dll into the following previously running processes: " + string.Join(", ",
                from p in injected select string.Format("{0} (PID {1})", Path.GetFileName(p.MainModule.FileName), p.Id));
            icon.ShowBalloonTip(5000);
        }

        static void OnProcessTimer(object sender, EventArgs e)
        {
            var candidates = scan();
            foreach (var p in candidates)
            {
                var thread = new Thread(InjectSoon);
                thread.Start(p);
            }
        }

        static void InjectSoon(object data)
        {
            var p = (Process)data;
            var pid = p.Id;
            var name = p.MainModule.ModuleName.ToLower();
            // Mark as handled so the next scan doesn't catch this again
            handledPids.Add(pid);
            Thread.Sleep(5000);
            if (p.HasExited) return;
            var res = Injector.Inject(pid, false);
            if (res == InjectResult.RETRY)
            {
                Thread.Sleep(5000);
                res = Injector.Inject(pid, true);
            }
            if (!handleInjectResult(res, name)) return;
            icon.BalloonTipTitle = "RInput injected";
            icon.BalloonTipText = string.Format("rinput.dll injected into process {0} (PID {1})", new object[] { name, pid });
            icon.ShowBalloonTip(5000);
        }

        static void updateAutostart(bool enable)
        {
            const string autopath = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
            RegistryKey key = Registry.CurrentUser.OpenSubKey(autopath, true);
            if (enable)
            {
                key.SetValue("rinput-tool", Application.ExecutablePath);
                reg.SetValue("Autostart", 1);
            }
            else
            {
                key.DeleteValue("rinput-tool");
                reg.SetValue("Autostart", 0);
            }
        }

        public static void updateProcesses(string[] names)
        {
            HashSet<string> set = new HashSet<string>(names);
            foreach (string k in configured.ToList<string>())
            {
                if (!set.Contains(k))
                {
                    configured.Remove(k);
                    reg.OpenSubKey("Instances", true).DeleteSubKey(k);
                }
            }
            foreach (string k in names)
            {
                if (!configured.Contains(k))
                {
                    configured.Add(k);
                    reg.OpenSubKey("Instances", true).CreateSubKey(k);
                }
            }
        }

        static void m_Show(object sender, EventArgs e)
        {
            if (form.IsDisposed) form = new ManageRInputs();
            form.setNames(configured.Cast<String>().ToArray());
            form.Show();
        }

        static void i_Show(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left) m_Show(sender, e);
        }

        static void m_ToggleAutostart(object sender, EventArgs e)
        {
            autostart.Checked = !autostart.Checked;
            updateAutostart(autostart.Checked);
        }

        static void m_Exit(object sender, EventArgs e)
        {
            icon.Dispose();
            timer.Stop();
            Application.Exit();
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            configured = new HashSet<string>();
            handledPids = new HashSet<int>();
            form = new ManageRInputs();
            icon = new NotifyIcon();
            icon.Icon = form.Icon;
            icon.Visible = true;
            icon.Text = "RInput-tool: right-click for options, left-click to show";
            icon.ContextMenu = initMenu();
            icon.MouseClick += i_Show;
            initReg();
            fetchReg();
            setupMonitor();
            setupExisting();
            Application.Run();
        }
    }
}

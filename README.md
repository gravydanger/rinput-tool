# RInput-tool

https://gitlab.com/gravydanger/rinput-tool

## Introduction

Some PC games (and similar programs) use changes in Windows's mouse cursor position to handle mouse input. This method of handling input can be a little unreliable with high-DPI mice, so someone created rinput.dll. This library overrides Windows mouse handling with its own, more accurate data based on raw mouse data (raw input = rinput).

rinput.dll is commonly used by running its injector tool "rinput.exe" and passing it the name of the target process. This is cumbersome and if you do it twice, there's a risk of crashes in your target program.

Enter RInput-tool -- a handy tool which allows you to maintain a list of programs you want to rinput-ify. It will automatically magick rinput.dll into these programs whenever they are running.

## How to use

After installing and first starting it, RInput-tool shows you its main window which is just a list of programs that it works on. The list is pre-loaded with "Star Wars Jedi Knight 2: Jedi Outcast" and "Star Wars Jedi Knight 3: Jedi Academy". To add new games, just choose "Add by path" and locate the game's main executable file, or if you know the filename, just enter it using "Enter name".

You can close the window once you are done setting up your list. RInput-tool will continue running in the background, and leave its logo (a black capital "R" in a yellow warning sign) in the system tray, from where you can re-open it by left clicking or open its context menu (e.g. for quitting RInput-tool) by right clicking.

When RInput-tool starts, it finds all programs already running that are on its list and installs rinput.dll into them; as long as you keep it running, newly started matching programs will be affected, too.

You will see a balloon popup whenever rinput.dll gets installed into a running program.

## Feedback/suggestions

If you have any issues with RInput-tool or want to suggest changes, please open an issue in the GitLab project.

## Credits

The repository includes a binary build of rinput.dll 1.44, courtesy of https://github.com/VolsandJezuz/Rinput-Library (where you will also find its source code).
